﻿using System;


namespace Task15
{
    internal class Program
    {
        private static void Main()
        {
            var st = new Student("Ivan", "Petrov");

            Engine.XMLSerialize(st);
            var stDeserializebelXml = Engine.XMLDeserialize();
            Console.WriteLine(stDeserializebelXml.GetFullName());

            Engine.BinarySerialize(st);
            var stDeserializebelBin = Engine.BinaryDeserialize();
            Console.WriteLine(stDeserializebelBin.GetFullName());

            Engine.SoapSerialize(st);
            var stDeserializebelSoap = Engine.SoapDeserialize();
            Console.WriteLine(stDeserializebelSoap.GetFullName());

            Console.ReadKey();
        }


    }
}
