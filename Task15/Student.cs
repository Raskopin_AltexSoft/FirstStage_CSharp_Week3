﻿using System;


namespace Task15
{
    [Serializable]
    public class Student
    {

        private readonly DateTime _from = DateTime.Now.AddYears(-120);
        private readonly DateTime _to = DateTime.Now.AddYears(-15);
        private DateTime _birthday;
        private DateTime _graduationDate;


        public Student()
        {
            
        }

        public Student(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime GraduationDate
        {
            get { return _graduationDate; }
            set {
                if (value > Birthday)
                {
                    _graduationDate = value;
                }
            }
        }

        public byte SchoolNumber { get; set; }

        public DateTime Birthday
        {
            get { return _birthday; }
            set
            {
                if (value >= _from && value <= _to)
                _birthday = value;
            }
        }
       
        public string GetFullName()
        {
            return FirstName + " " + LastName;
        }
    }
}
