﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Xml.Serialization;

namespace Task15
{
    static class Engine
    {
        public static void XMLSerialize(Student student)
        {
            var serializer = new XmlSerializer(typeof(Student));
            using (var stream = new FileStream("Serialization.xml", FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                serializer.Serialize(stream, student);
            }
        }

        public static Student XMLDeserialize()
        {
            var serializer = new XmlSerializer(typeof(Student));
            Student student; 
            using (var stream = new FileStream("Serialization.xml", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                student = (Student)serializer.Deserialize(stream) ;
            }
            return student;
        }

        public static void BinarySerialize(Student student)
        {
            using (var stream = new FileStream("Serialization.dat", FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                var serializer = new BinaryFormatter();
                serializer.Serialize(stream, student);
            }
        }

        public static Student BinaryDeserialize()
        {
            using (var stream = new FileStream("Serialization.dat", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var serializer = new BinaryFormatter();
                return (Student)serializer.Deserialize(stream);
            }
        }

        public static void SoapSerialize(Student student)
        {
            using (var stream = new FileStream("Serialization.soap", FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                var serializer = new SoapFormatter();
                serializer.Serialize(stream, student);
            }
        }

        public static Student SoapDeserialize()
        {
            using (var stream = new FileStream("Serialization.soap", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var serializer = new SoapFormatter();
                return (Student)serializer.Deserialize(stream); ;
            }
        }
    }
}
