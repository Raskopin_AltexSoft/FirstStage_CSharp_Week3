﻿using System;
using System.Collections.Generic;


namespace Task12
{
    internal class Engine
    {
        private static Engine _instance;

        private Engine()
        { }

        public static Engine GetInstance()
        {
            return _instance ?? (_instance = new Engine());
        }

        readonly List<Person> _persons = new List<Person>();

        public List<Person> SetList()
        {
            var makarenko = new Teacher("Anton", "Makarenko", new DateTime(1888, 1, 13), "Pedagogical", "Russian Empire", "Bilopillia", "Center street", 7);
            //for this object and object above we later will check gethashcode and equal
            var makarenkoRepeat = new Teacher("Anton", "Makarenko", new DateTime(1888, 1, 13), "Pedagogical", "Russian Empire", "Bilopillia", "Center street", 7);
            //same teacher with wrong Address for chek equals method
            var wrongMakarenko = new Teacher("Anton", "Makarenko", new DateTime(1888, 1, 13), "Pedagogical", "Russian Empire", "Bilopillia", "Center street", 8);
            var einstein = new Teacher("Albert ", "Einstein", new DateTime(1879, 4, 18), "Physical", "German Empire", "Ulm", "Kaiser street", 10, 17);
            var newton = new Teacher("Isaac", "Newton", new DateTime(1642, 3, 20), "Physical", "England", "Woolsthorpe", "Shakespeare street", 5);
            var confucius = new Teacher("Confucius", "", new DateTime(551, 09, 28), "Philosophy", "China", "Zou", "Center", 1);

            var tolstoy = new Person("Leo", "Tolstoy", new DateTime(1828, 9, 9), "Russian Impire", "Yasnaya Polyana", "Sunny street", 20);
            //same person with wrong birthday for check equals and gethashcode method
            var wrongTolstoy = new Person("Leo", "Tolstoy", new DateTime(1828, 9, 10), "Russian Impire", "Yasnaya Polyana", "Sunny street", 20);
            //for this person and object above we later will check gethashcode and equal
            var tolstoyRepeat = new Person("Leo", "Tolstoy", new DateTime(1828, 9, 9), "Russian Impire", "Yasnaya Polyana", "Sunny street", 20);
            var dostoeyevsky = new Person("Fyodor", "Dostoyevsky", new DateTime(1846, 11, 11), "Russian Impire", "Moscow", "Tverskaya street", 50, 8);
            var chekhov = new Person("Anton", "Chekhov", new DateTime(1860, 7, 15), "Russian Impire", "Taganrog", "Green boulevard", 2);
            var faulkner = new Person("William", "Faulkner", new DateTime(1897, 9, 25), "USA", "New Albany", "Second Avenue", 91, 55);

            var afanasenko = new Student("Vasya", "Afanasenko", new DateTime(1988, 1, 25), "Pedagogical", makarenko, "Ukraine", "Kremenchuk", "Sobornaya street", 9, 5, 5);
            var goldarev = new Student("Aleksandr", "Goldarev", new DateTime(1992, 4, 2), "Pedagogical", makarenko, "Ukraine", "Kremenchuk", "Pushkina boulevard", 2, 5);
            var goldarev2 = new Student("Aleksandr2", "Goldarev", new DateTime(1992, 4, 2), "Pedagogical", makarenko, "Ukraine", "Kremenchuk", "Pushkina boulevard", 2, 5);
            var potavec = new Student("Sofia", "Potavec", new DateTime(1970, 10, 28), "Pedagogical", wrongMakarenko, "Ukraine", "Kremenchuk", "Second Avenue", 10, 15);
            var petrova = new Student("Olga", "Petrova", new DateTime(1989, 2, 15), "Physical", einstein, "Ukraine", "Poltava", "Pochtova ploscha", 13, 7);
            var kotikov = new Student("Boris", "Kotikov", new DateTime(1992, 7, 25), "Physical", einstein, "Ukraine", "Poltava", "Green street", 9, 8);
            var rolina = new Student("Elizaveta", "Rolina", new DateTime(1993, 1, 13), "Philosophy", confucius, "Ukraine", "Globino", "Kolhoznaya street", 11, 102);
            var korjenko = new Student("Mikhail", "Korjenko", new DateTime(1985, 5, 10), "Philosophy", confucius, "Ukraine", "Dnepr", "Main", 3, 17);
            var alekseev = new Student("Bogdan", "Alekseev", new DateTime(1990, 5, 17), "Physical", newton, "Ukraine", "Dnepr", "Second tupik", 22, 1);
            var ivanov = new Student("Vasya", "Ivanov", new DateTime(1988, 8, 5), "Pedagogical", makarenko, "Ukraine", "Kiev", "Kreschatik street", 5, 7);

            _persons.Add(makarenko);
            _persons.Add(makarenkoRepeat);
            _persons.Add(wrongMakarenko);
            _persons.Add(einstein);
            _persons.Add(newton);
            _persons.Add(confucius);

            _persons.Add(tolstoy);
            _persons.Add(wrongTolstoy);
            _persons.Add(tolstoyRepeat);
            _persons.Add(dostoeyevsky);
            _persons.Add(chekhov);
            _persons.Add(faulkner);

            _persons.Add(afanasenko);
            _persons.Add(goldarev);
            _persons.Add(goldarev2);
            _persons.Add(potavec);
            _persons.Add(petrova);
            _persons.Add(kotikov);
            _persons.Add(rolina);
            _persons.Add(korjenko);
            _persons.Add(alekseev);
            _persons.Add(ivanov);

            return _persons;
        }

        public Persons SetPersons(List<Person> persons)
        {
            var customPersonsList = new Persons();
            foreach (var person in persons)
            {
                customPersonsList.Add(person);
            }
            return customPersonsList;
        }
    }
}
