﻿namespace Task12
{
    internal interface IPrintable
    {
        void Print();
    }
}
