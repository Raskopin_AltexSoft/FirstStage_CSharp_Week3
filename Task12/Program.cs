﻿using System;
using System.Collections.Generic;


namespace Task12
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var engine = Engine.GetInstance();
            var persons = engine.SetList();
            var i = 0;
            var students = new List<Student>();
            foreach (var person in persons)
            {
                if (person.GetType() == typeof (Student))
                    students.Add((Student)person);
                Console.WriteLine("{0}: {1}", i++, person);
            }

            Console.WriteLine("Impicit method Print: ");
            persons[0].Print();

            Console.WriteLine("Explicit method Print");
            ((IPrintable) persons[0]).Print();

            students.Sort();
            Console.WriteLine("Sorted list of students by default sort method:");
            foreach (var student in students)
            {
                Console.WriteLine(student);
            }

            Student.SortByBirthday(students);
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Sorted list of students by birthday with custom sort method:");
            Console.ResetColor();
            foreach (var student in students)
            {
                Console.WriteLine(student);
            }

            Student.SortByFirstName(students);
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Sorted list of students by FirstName with custom sort method:");
            Console.ResetColor();
            foreach (var student in students)
            {
                Console.WriteLine(student);
            }

            Student.SortByLastName(students);
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Sorted list of students by LastName with custom sort method:");
            Console.ResetColor();
            foreach (var student in students)
            {
                Console.WriteLine(student);
            }

            Student.SortByCourse(students);
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Sorted list of students by Course with custom sort method:");
            Console.ResetColor();
            foreach (var student in students)
            {
                Console.WriteLine(student);
            }

            Student.SortByFaculty(students);
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Sorted list of students by Faculty with custom sort method:");
            Console.ResetColor();
            foreach (var student in students)
            {
                Console.WriteLine(student);
            }

            var customPersonsCollection = engine.SetPersons(persons);
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Check foreach loop for our custom collection:");
            Console.ResetColor();
            var count = 0;
            foreach (var person in customPersonsCollection)
            {
                if (person.GetType() == typeof(Person) || person.GetType() == typeof(Student))
                    Console.WriteLine(count + ". " + person);
                count++;
            }

            Console.ReadKey();
        }
    }
}
