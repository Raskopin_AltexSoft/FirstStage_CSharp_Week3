﻿using System;
using System.Collections.Generic;


namespace Task12
{
    class Teacher: Person, IPrintable, IComparable<Student>
    {
        public string Subject { get; }
        private readonly List<Student> _students = new List<Student>();

        public Teacher(string firstName, string lastName, DateTime birthday, string subject)
            : base(firstName, lastName, birthday)
        {
            Subject = subject;
        }

        //constructor with Address set
        public Teacher(string firstName, string lastName, DateTime birthday, string subject, string country, string town, string street, short buildingNumber, short apartmentNumber = 0)
            : base(firstName, lastName, birthday, country, town, street, buildingNumber, apartmentNumber)
        {
            Subject = subject;
        }

        //constructor with class Address and list of _students in arguments
        public Teacher(string firstName, string lastName, DateTime birthday, Address address, string subject, List<Student> students)
            : base(firstName, lastName, birthday, address)
        {
            Subject = subject;
            this._students = students;
        }

        //add student to _students list
        public void AddStudent(Student student)
        {
            _students.Add(student);
        }

        //return _students list
        public List<Student> GetStudents()
        {
            return _students;
        }

        //override virtual method ToString
        public int CompareTo(Student other)
        {
            return 0;
        }

        public override string ToString()
        {
            return FullName + "; Subject: " + Subject + "; Number of _students: " + _students.Count;
        }

        //override virtual method GetHashCode. return xor of properties hashes that take part in Equals method
        public override int GetHashCode()
        {
            return FullName.GetHashCode() ^ Birthday.GetHashCode() ^ Address.FullAddres.GetHashCode() ^ Subject.GetHashCode();
        }

        void IPrintable.Print()
        {
            Console.WriteLine("From IPrintable: " + ToString());
        }

        //define method Clone for ICloneable interface
        public override object Clone()
        {
            return new Teacher(this.FirstName, this.LastName, this.Birthday, this.Address, this.Subject, this._students);
        }

    }
}
