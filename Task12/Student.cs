﻿using System;
using System.Collections.Generic;


namespace Task12
{
    internal class Student: Person,IPrintable, IComparable<Student>
    {
        private byte _course;
        public string Faculty { get; set; }

        public byte Course
        {
            get { return _course; }
            set
            {
                _course = 1;
                if (value > 0 && value < 6)
                {
                    _course = value;
                }
            }
        }

        public Teacher Teacher { get; set; }

        //constructor with faculty and teacher in arguments
        public Student(string firstName, string lastName, DateTime birthday, string faculty, Teacher teacher, byte course = 1)
            : base(firstName, lastName, birthday)
        {
            Faculty = faculty;
            Course = course;
            Teacher = teacher;
            teacher.AddStudent(this);
        }

        //constructor with Address set
        public Student(string firstName, string lastName, DateTime birthday, string faculty, Teacher teacher,
            string country, string town, string street, short buildingNumber, short apartmentNumber = 0, byte course = 1)
            : base(firstName, lastName, birthday, country, town, street, buildingNumber, apartmentNumber)
        {
            Faculty = faculty;
            Course = course;
            Teacher = teacher;
            teacher.AddStudent(this);
        }

        //constructor with class Address in arguments.This constructor for clone method
        public Student(string firstName, string lastName, DateTime birthday, Address address, string faculty, Teacher teacher, byte course = 1)
        : base(firstName, lastName, birthday, address)
        {
            Faculty = faculty;
            Course = course;
            Teacher = teacher;
        }

        public int CompareTo(Student student)
        {
            return string.Compare(FirstName, student.FirstName, StringComparison.Ordinal);
        }

        //override virtual method ToString
        public override string ToString()
        {
            return FullName + "; Birhtday: " + Birthday +"; Faculty: " + Faculty + "; Teacher: " + Teacher.FullName;
        }

        //override virtual method GetHashCode. return xor of properties hashes that take part in Equals method
        public override int GetHashCode()
        {
            return FullName.GetHashCode() ^ Birthday.GetHashCode() ^ Address.FullAddres.GetHashCode() ^ Faculty.GetHashCode();
        }

        void IPrintable.Print()
        {
            Console.WriteLine("From IPrintable: " + ToString());
        }

        //define method Clone for ICloneable interface
        public override object Clone()
        {
            var p = new Student(this.FirstName, this.LastName, this.Birthday, this.Address, this.Faculty, this.Teacher, this.Course);
            return p;
        }

        private class StudentComparer: IComparer<Student>
        {
            readonly StudentProperties _property;
            public StudentComparer(StudentProperties property)
            {
                this._property = property;
            }
            public int Compare(Student student1, Student student2)
            {
                switch (_property)
                {
                    case StudentProperties.FirsName:
                        return string.Compare(student1.FirstName, student2.FirstName, StringComparison.Ordinal);
                    case StudentProperties.LastName:
                        return string.Compare(student1.LastName, student2.LastName, StringComparison.Ordinal);
                    case StudentProperties.Birthday:
                        return DateTime.Compare(student1.Birthday, student2.Birthday); 
                    case StudentProperties.Course:
                    {
                        if (student1.Course == student2.Course)
                            return 0;
                        return student1.Course > student2.Course ? 1 : -1;
                    }
                    case StudentProperties.Faculty:
                         return string.Compare(student1.Faculty, student2.Faculty, StringComparison.Ordinal);
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public static List<Student> SortByBirthday(List<Student> students)
        {
            var sc = new StudentComparer(StudentProperties.Birthday);
            students.Sort(sc);
            return students;
        }

        public static List<Student> SortByFirstName(List<Student> students)
        {
            var sc = new StudentComparer(StudentProperties.FirsName);
            students.Sort(sc);
            return students;
        }

        public static List<Student> SortByLastName(List<Student> students)
        {
            var sc = new StudentComparer(StudentProperties.LastName);
            students.Sort(sc);
            return students;
        }

        public static List<Student> SortByCourse(List<Student> students)
        {
            var sc = new StudentComparer(StudentProperties.Course);
            students.Sort(sc);
            return students;
        }

        public static List<Student> SortByFaculty(List<Student> students)
        {
            var sc = new StudentComparer(StudentProperties.Faculty);
            students.Sort(sc);
            return students;
        }
    }
}
