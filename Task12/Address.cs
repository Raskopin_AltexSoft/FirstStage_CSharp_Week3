﻿namespace Task12
{
    internal class Address
    {
        public string Country { get; set; }
        public string Town { get; set; }
        public string Street { get; set; }
        public short BuildingNumber { get; set; }
        public short ApartmentNumber { get; set; }
        public string FullAddres
        {
            get
            {
                if (ApartmentNumber > 0)
                {
                    return ApartmentNumber + BuildingNumber + Street + Town + Country;
                }
                return BuildingNumber + Street + Town + Country;
            }
        }
    }
}
