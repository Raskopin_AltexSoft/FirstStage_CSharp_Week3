﻿using System.Collections;
using System.Collections.Generic;


namespace Task12
{
    internal class Persons: IEnumerable<Person>, IEnumerator<Person>
    {
        readonly List<Person> _persons = new List<Person>();

        int _position = -1;

        public void Add(Person person)
        {
            _persons.Add(person);
        }

        public void Remove(Person person)
        {
            _persons.Remove(person);
        }


        public IEnumerator<Person> GetEnumerator()
        {
            //return this;
            foreach (var person in _persons)
            {
                yield return person;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Dispose()
        {
            Reset();
        }

        public bool MoveNext()
        {
            if (_position >= _persons.Count - 1) return false;
            _position++;
            return true;
        }

        public void Reset()
        {
            _position = -1;
        }

        public Person Current
        {
            get
            {
                return _persons[_position];
            }
        }

        object IEnumerator.Current
        {
            get { return Current; }
        }
    }
}
