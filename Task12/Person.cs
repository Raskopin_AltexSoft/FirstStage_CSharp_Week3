﻿using System;


namespace Task12
{
    internal class Person: IPrintable, ICloneable, IDisposable
    {
        protected readonly Address Address;
        public bool IsSet { get; set; }
        public bool IsAddresSet { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get { return FirstName + " " + LastName; } }
        public DateTime Birthday { get; }

        //constructor without Address set
        public Person(string firstName, string lastName, DateTime birthday)
        {
            IsSet = false;
            FirstName = firstName;
            LastName = lastName;
            Birthday = birthday;
        }

        //constructor with Address set
        public Person(string firstName, string lastName, DateTime birthday, string country, string town, string street, short buildingNumber, short apartmentNumber = 0)
            : this(firstName, lastName, birthday)
        {
            Address = new Address
            {
                Country = country,
                Town = town,
                Street = street,
                BuildingNumber = buildingNumber,
                ApartmentNumber = apartmentNumber
            };
            IsSet = true;
        }


        //construtor with class Address as argument
        public Person(string firstName, string lastName, DateTime birthday, Address address)
            : this(firstName, lastName, birthday)
        {
            this.Address = address;
        }

        //method for Address changing
        public void ChangeAddress(string country, string town, string street, short buildingNumber, short apartmentNumber = 0)
        {
            Address.Country = country;
            Address.Town = town;
            Address.Street = street;
            Address.BuildingNumber = buildingNumber;
            Address.ApartmentNumber = apartmentNumber;
        }


        //override virtual method ToString
        public override string ToString()
        {
            return FullName;
        }


        //override virtual method GetHashCode. return xor of properties hashes that take part in Equals method
        public override int GetHashCode()
        {
            return FullName.GetHashCode() ^ Birthday.GetHashCode() ^ Address.FullAddres.GetHashCode();
        }

        public void Dispose()
        {
            Console.WriteLine("Method Dispose has called. ");
        }

        //explicit realized of IPrintable interface
        void IPrintable.Print()
        {
            Console.WriteLine("From IPrintable: " + ToString());
        }

        //implicit realized of IPrintable interface
        public void Print()
        {
            Console.WriteLine(ToString() );
        }

        //define method Clone for ICloneable interface
        public virtual object Clone()
        {
            var p = new Person(this.FirstName, this.LastName, this.Birthday, this.Address);
            return p;
        }
    }
}
