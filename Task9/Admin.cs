﻿using System;


namespace Task9
{
    internal class Admin: IReceiver
    {
        public string Name { get; }

        public virtual void GetMessage(string messageText)
        {
            Console.WriteLine("{0} got the message: {1}", Name, messageText);
        }

        public Admin(string name)
        {
            Name = name;
        }
    }
}
