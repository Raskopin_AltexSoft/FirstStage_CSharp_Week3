﻿using System;


namespace Task9
{
    internal delegate void MessageHandler(string messageText);

    internal delegate void NewUserHandler(string text);

    internal delegate void NewMessageFromeUserHandler(string text);

    internal class Program
    {
        private static void Main(string[] args)
        {
            Engine.SetUsers();
            Engine.Authorization();
            Console.ReadKey();
        }
    }
}
