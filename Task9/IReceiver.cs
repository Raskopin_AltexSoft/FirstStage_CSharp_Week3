﻿namespace Task9
{
    internal interface IReceiver
    {
        void GetMessage(string messageText);
        string Name { get; }
    }
}
