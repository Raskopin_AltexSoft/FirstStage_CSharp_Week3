﻿using System;
using System.Collections.Generic;


namespace Task9
{
    internal class Server
    {

        public event MessageHandler MessageSend;

        public event NewUserHandler NewUserAdded;

        public event NewMessageFromeUserHandler NewMessageFromUser;

        private static Server _instance;

        private readonly List<IReceiver> _users = new List<IReceiver>();

        private Server()
        {
            NewUserAdded += Console.WriteLine;
            NewMessageFromUser += Console.WriteLine;
        }

        public static Server GetInstance()
        {
            return _instance ?? (_instance = new Server());
        }
        
        public  List<IReceiver> GetAllUsers(IReceiver current = null)
        {
            if (current == null) return _users;
            var usersWithoutCurrent = new List<IReceiver>();
            foreach (var user in _users)
            {
                if (user.Name != current.Name)
                    usersWithoutCurrent.Add(user);
            }
            return usersWithoutCurrent;
        }

        public  void AddNewUser(IReceiver user)
        {
            if (user == null) return;
            _users.Add(user);
            NewUserAdded("New user " + user.Name + "  just has added.");
        }

        public void SendMessage(List<IReceiver> receivers, IReceiver user, string messageText)
        {
            if (receivers == null || user == null) return;
            NewMessageFromUser("User " + user.Name + " just send a message.");
            ReceiversSubscription(receivers);
            MessageSend(messageText);
            UnsubscripeAllHandlers();
        }

        private void ReceiversSubscription(List<IReceiver> receivers)
        {
            if (receivers == null) return;
            foreach (var receiver in receivers)
            {
                MessageSend += receiver.GetMessage;
            }
        }

        private void UnsubscripeAllHandlers()
        {
            if (MessageSend == null) return;
            foreach (var d in MessageSend.GetInvocationList())
                MessageSend -= (d as MessageHandler);
        }

        public IReceiver IfUserWithNameExist(string name)
        {
            foreach (var user in _users)
            {
                if (user.Name == name)
                    return user;
            }
            return null;
        }
    }
}
