﻿using System;
using System.Collections.Generic;


namespace Task9
{
    internal static class Engine
    {
        static IReceiver _currentUser;

        static Server _server;

        public static void SetUsers()
        {
            _server  = Server.GetInstance();
            _server.AddNewUser(new User("Alex"));
            _server.AddNewUser(new User("Mariya"));
            _server.AddNewUser(new User("Ivan"));
            _server.AddNewUser(new Admin("Admin"));
        }

        public static void Authorization()
        {
            Console.WriteLine("Please, enter your name:");
            _currentUser = _server.IfUserWithNameExist(Console.ReadLine());
            if (_currentUser == null)
            {
                Console.WriteLine("Such user does not exist.");
                if (Continue())
                {
                    Authorization();
                }
                return;
            }
            Console.WriteLine("Hello, {0}!", _currentUser.Name);
            Console.WriteLine( "You can send message to this users:");
            var choose = ChooseUser();
            Console.WriteLine("Input text:");
            var text = Console.ReadLine();
            switch (choose)
            {
                case -2:
                    return;
                case -1:
                    _server.SendMessage(_server.GetAllUsers(_currentUser), _currentUser,text);
                    break;
                default:
                    _server.SendMessage(new List<IReceiver>() { _server.GetAllUsers(_currentUser)[choose]}, _currentUser,  text);
                    break;
            }
        }
        
        //this method realize user`s choose of message receiver
        public static int ChooseUser()
        {
            var allowSymbols = new List<string> { "X", "x", "A", "a" };
            var i = 0;
            foreach (var user in _server.GetAllUsers(_currentUser))
            {
                i++;
                allowSymbols.Add(i.ToString());
                Console.WriteLine("{0}. {1};", i, user.Name);
            }
            Console.WriteLine("Or press \"A\" for send message all users.");
            string output;
            Console.WriteLine("Press X to exit this menu.");
            while (!allowSymbols.Contains(output = Console.ReadLine()))
            {
                Console.WriteLine("Value is wrong. Please, try again:");
            }
            if (output == "A" || output == "a")
                return -1;
            return int.Parse(output) - 1;
        }

        //this method handle user`s answer to question continue work with menu or no
        private static bool Continue()
        {
            Console.WriteLine("Do you want to continue? Y/N");
            var answer = Console.ReadLine();
            if (answer != "y" && answer != "Y" && answer != "N" && answer != "n")
            {
                Continue();
            }
            return answer == "Y" || answer == "y";
        }
    }
}
