﻿using System;
using System.Collections.Generic;


namespace Task11_1
{
    class Teacher: Person
    {
        public string Subject { get; }
        private readonly List<Student> _students = new List<Student>();

        public Teacher(string firstName, string lastName, DateTime birthday, string subject)
            : base(firstName, lastName, birthday)
        {
            Subject = subject;
        }

        //constructor with Address set
        public Teacher(string firstName, string lastName, DateTime birthday, string subject, string country, string town, string street, short buildingNumber, short apartmentNumber = 0)
            : base(firstName, lastName, birthday, country, town, street, buildingNumber, apartmentNumber)
        {
            Subject = subject;
        }

        //add student to _students list
        public void AddStudent(Student student)
        {
            _students.Add(student);
        }

        //return _students list
        public List<Student> GetStudents()
        {
            return _students;
        }

        //override virtual method ToString
        public override string ToString()
        {
            return FullName + "; Subject: " + Subject + "; Number of _students: " + _students.Count;
        }

        //override virtual method GetHashCode. return xor of properties hashes that take part in Equals method
        public override int GetHashCode()
        {
            return FullName.GetHashCode() ^ Birthday.GetHashCode() ^ Address.FullAddres.GetHashCode() ^ Subject.GetHashCode();
        }

        public static bool operator ==(Teacher p1, Teacher p2)
        {
            return (object)p1 != null && (object)p2 != null &&  p1.FullName == p2.FullName && p1.Address.FullAddres == p2.Address.FullAddres 
                   && p1.Birthday == p2.Birthday && p1.Subject == p2.Subject;
        }

        public static bool operator !=(Teacher p1, Teacher p2)
        {
            return (object)p1 == null || (object)p2 == null || p1.FullName != p2.FullName || p1.Address.FullAddres != p2.Address.FullAddres 
                   || p1.Birthday != p2.Birthday || p1.Subject != p2.Subject;
        }
    }
}
