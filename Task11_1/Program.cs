﻿using System;


namespace Task11_1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var engine = Engine.GetInstance();
            var persons = engine.SetList();
            var i = 0;
            foreach (var person in persons)
            {
                Console.WriteLine("{0}: {1}",i++,person);
            }
            Console.WriteLine("Compare person");
            Console.WriteLine("Does {0} and {1} the same: {2}", persons[6].FullName + " " + persons[6].Birthday, persons[7].FullName + " " + persons[7].Birthday, persons[6] == persons[7]);
            Console.WriteLine("Compare student");
            Console.WriteLine("Does {0} and {1} the same: {2}", persons[12].FullName, persons[13].FullName, (Student)persons[12] == (Student)persons[13]);
            Console.WriteLine("Compare teacher");
            Console.WriteLine("Does {0} and {1} the same: {2}", persons[0].FullName, persons[1].FullName, (Teacher)persons[0] == (Teacher)persons[1]);
            Console.ReadKey();
        }
    }
}
