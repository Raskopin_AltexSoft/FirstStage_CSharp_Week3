﻿using System;


namespace Task11_1
{
    class Student: Person
    {
        private byte _course;
        public string Faculty { get; set; }

        public byte Course
        {
            get { return _course; }
            set
            {
                _course = 1;
                if (value > 0 && value < 6)
                {
                    _course = value;
                }
            }
        }

        public Teacher Teacher { get; set; }

        //constructor with faculty and teacher in arguments
        public Student(string firstName, string lastName, DateTime birthday, string faculty, Teacher teacher, byte course = 1)
            : base(firstName, lastName, birthday)
        {
            Faculty = faculty;
            Course = course;
            Teacher = teacher;
            teacher.AddStudent(this);
        }

        //constructor with Address set
        public Student(string firstName, string lastName, DateTime birthday, string faculty, Teacher teacher,
            string country, string town, string street, short buildingNumber, short apartmentNumber = 0, byte course = 1)
            : base(firstName, lastName, birthday, country, town, street, buildingNumber, apartmentNumber)
        {
            Faculty = faculty;
            Course = course;
            Teacher = teacher;
            teacher.AddStudent(this);
        }


        //override virtual method ToString
        public override string ToString()
        {
            return FullName + "; Faculty: " + Faculty + "; Teacher: " + Teacher.FullName;
        }

        //override virtual method GetHashCode. return xor of properties hashes that take part in Equals method
        public override int GetHashCode()
        {
            return FullName.GetHashCode() ^ Birthday.GetHashCode() ^ Address.FullAddres.GetHashCode() ^ Faculty.GetHashCode();
        }

        public static bool operator ==(Student p1, Student p2)
        {
            return (object)p1 != null && (object)p2 != null && p1.FullName == p2.FullName && p1.Address.FullAddres == p2.Address.FullAddres 
                   && p1.Birthday == p2.Birthday && p1.Faculty==p2.Faculty && p1.Course == p2.Course && p1.Teacher.FullName == p2.Teacher.FullName;
        }

        public static bool operator !=(Student p1, Student p2)
        {
            return (object)p1 == null || (object)p2 == null || p1.FullName != p2.FullName || p1.Address.FullAddres != p2.Address.FullAddres 
                   || p1.Birthday != p2.Birthday || p1.Birthday != p2.Birthday || p1.Faculty != p2.Faculty || p1.Course != p2.Course || p1.Teacher.FullName != p2.Teacher.FullName;
        }
    }
}
