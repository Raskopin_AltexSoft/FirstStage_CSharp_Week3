﻿using System;


namespace Task11_1
{
    class Person
    {
        protected readonly Address Address;
        public bool IsSet { get; set; }
        public bool IsAddresSet { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get { return FirstName + " " + LastName; } }
        public DateTime Birthday { get; }

        //constructor without Address set
        public Person(string firstName, string lastName, DateTime birthday)
        {
            IsSet = false;
            FirstName = firstName;
            LastName = lastName;
            Birthday = birthday;
        }

        //constructor with Address set
        public Person(string firstName, string lastName, DateTime birthday, string country, string town, string street, short buildingNumber, short apartmentNumber = 0)
            : this(firstName, lastName, birthday)
        {
            Address = new Address
            {
                Country = country,
                Town = town,
                Street = street,
                BuildingNumber = buildingNumber,
                ApartmentNumber = apartmentNumber
            };
            IsSet = true;
        }

        //method for Address changing
        public void ChangeAddress(string country, string town, string street, short buildingNumber, short apartmentNumber = 0)
        {
            Address.Country = country;
            Address.Town = town;
            Address.Street = street;
            Address.BuildingNumber = buildingNumber;
            Address.ApartmentNumber = apartmentNumber;
        }

        //override virtual method ToString
        public override string ToString()
        {
            return FullName;
        }

        //override virtual method GetHashCode. return xor of properties hashes that take part in Equals method
        public override int GetHashCode()
        {
            return FullName.GetHashCode() ^ Birthday.GetHashCode() ^ Address.FullAddres.GetHashCode();
        }

        public static bool operator == (Person p1, Person p2)
        {
            return (object)p1 != null && (object)p2 != null && p1.FullName == p2.FullName && p1.Address.FullAddres == p2.Address.FullAddres && p1.Birthday == p2.Birthday;
        }

        public static bool operator !=(Person p1, Person p2)
        {
            return (object)p1 == null || (object)p2 == null || p1.FullName != p2.FullName || p1.Address.FullAddres != p2.Address.FullAddres || p1.Birthday != p2.Birthday;
        }
    }
}
