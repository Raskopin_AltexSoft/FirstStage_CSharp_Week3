﻿using System;
using System.Collections.Generic;
using System.IO;


namespace Task10_1
{
    internal static class Engine
    {
        public static void GetWatchedDisk()
        {
            var disksList = CustomWatcher.GetInstance().GetDisksList();
            if ( disksList!=null && disksList.Count>0 )
            {
                var allowSymbols = new List<string> { "X", "x" };
                Console.WriteLine( "Choose disk:");
                var i = 1;
                foreach (var diskName in disksList)
                {
                    Console.WriteLine("{0}. {1}", i, diskName.ToString());
                    allowSymbols.Add((i++).ToString());
                }
                Console.WriteLine("Press X to exit.");
                string output;
                while (!allowSymbols.Contains(output = Console.ReadLine()))
                {
                    Console.WriteLine("Value is wrong. Please, try again:");
                }
                int diskNumber;
                if (!int.TryParse(output, out diskNumber)) return;
                var dir = new DirectoryInfo(Directory.GetCurrentDirectory() + @"\..\..\folder\");
                if (!dir.Exists)
                    dir.Create();
                CreateOrOpenFile(Directory.GetCurrentDirectory() + @"\..\..\folder\");
                Console.WriteLine("Input file extension:");
                var fileExtension = Console.ReadLine();
                Console.WriteLine("Files watching is start.");
                CustomWatcher.GetInstance().RunWatcher(disksList[int.Parse(output) - 1].Name.Substring(0, 2), fileExtension);
            }
            else
            {
                Console.WriteLine("Something wrong with filesystme");
            }
        }

        private static void CreateOrOpenFile(string path)
        {
            var file = new FileInfo(path + "log.txt");
            if (!file.Exists)
                file.Create().Close();
        }
    }
}
