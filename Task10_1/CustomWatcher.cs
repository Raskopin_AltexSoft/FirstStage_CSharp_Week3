﻿using System;
using System.Collections.Generic;
using System.IO;


namespace Task10_1
{
    internal class CustomWatcher
    {
        private static CustomWatcher _instance;

        private readonly FileSystemWatcher _watcher;

        private CustomWatcher()
        {
            _watcher = new FileSystemWatcher();
            _watcher.Deleted += HandlerOnDelete;
            _watcher.Created += HandlerOnCreate;
            _watcher.Changed += HandlerOnChanged;
            _watcher.Renamed += HandlerOnRenamed;
        }

        public static CustomWatcher GetInstance()
        {
            return _instance ?? (_instance = new CustomWatcher());
        }

        public void RunWatcher(string path, string filter)
        {
            Console.WriteLine( path );
            _watcher.Path = path;
            _watcher.Filter = "*." + filter;
            _watcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            _watcher.EnableRaisingEvents = true;
        }

        public List<DriveInfo> GetDisksList()
        {
            var allDrives = new List<DriveInfo>();
            foreach (var driver in DriveInfo.GetDrives())
            {
                if (driver.DriveType == DriveType.Fixed || driver.DriveType == DriveType.Removable)
                {
                    allDrives.Add(driver);
                }
            }
            return allDrives;
        }

        private void HandlerOnDelete(object source, FileSystemEventArgs e)
        {
            if (File.Exists(Directory.GetCurrentDirectory() + @"\..\..\folder\" + "log.txt"))
            {
                var sw = new StreamWriter(Directory.GetCurrentDirectory() + @"\..\..\folder\" + "log.txt", true);
                sw.WriteLine(DateTime.Now+ " File " + e.FullPath + " was deleted.");
                sw.Close();
            }
            Console.WriteLine("File {0} was deleted.", e.FullPath);
        }

        private void HandlerOnCreate(object source, FileSystemEventArgs e)
        {
            if (File.Exists(Directory.GetCurrentDirectory() + @"\..\..\folder\" + "log.txt"))
            {
                var sw = new StreamWriter(Directory.GetCurrentDirectory() + @"\..\..\folder\" + "log.txt", true);
                sw.WriteLine(DateTime.Now + " File " + e.FullPath + " was created.");
                sw.Close();
            }
            Console.WriteLine("File {0} was created.", e.FullPath);
        }

        private void HandlerOnRenamed(object source, FileSystemEventArgs e)
        {
            if (File.Exists(Directory.GetCurrentDirectory() + @"\..\..\folder\" + "log.txt"))
            {
                var sw = new StreamWriter(Directory.GetCurrentDirectory() + @"\..\..\folder\" + "log.txt",true);
                sw.WriteLine(DateTime.Now + " File " + e.FullPath + " was renamed.");
                sw.Close();
            }
            Console.WriteLine("File {0} was renamed.", e.FullPath);
        }

        private void HandlerOnChanged(object source, FileSystemEventArgs e)
        {
            if (File.Exists(Directory.GetCurrentDirectory() + @"\..\..\folder\" + "log.txt"))
            {
                var sw = new StreamWriter(Directory.GetCurrentDirectory() + @"\..\..\folder\" + "log.txt", true);
                sw.WriteLine(DateTime.Now + " File " + e.FullPath + " was changed.");
                sw.Close();
            }
            Console.WriteLine("File {0} was changed.", e.FullPath);
        }
    }
}
