﻿using System;


namespace Task13
{
    internal class CustomComparer: ICustomCompare
    {
        int ICustomCompare.CustomCompare<T>(T obj1, T obj2)
        {
            return string.Compare(obj1.ToString(), obj2.ToString(), StringComparison.Ordinal);
        }
    }
}
