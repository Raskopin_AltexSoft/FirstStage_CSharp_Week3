﻿namespace Task13
{
    internal interface ICustomCompare
    {
        int CustomCompare<T>(T obj1, T obj2);
    }
}
