﻿using System;
using System.Collections.Generic;


namespace Task13
{
    public delegate int CustomComparison<in T>(T x, T y);

    internal class Program
    {
        private static void Main(string[] args)
        {
            var list = new List<string>() {"b", "xoh5o", "ams"};
            var sortList = list.EnumerableExtend(new CustomComparer());
            foreach (var i in sortList)
            {
                Console.WriteLine(i);
            }

            var list2 = new List<string>() { "b", "xo", "ahs" };
            var comp = new CustomComparison<string>(EnumerableExtendor.ObjectsCompare);
            var sortList2 = list2.EnumerableExtendByDelegate(comp);
            foreach (var i in sortList2)
            {
                Console.WriteLine(i);
            }
            Console.ReadKey();
        }
    }
}
