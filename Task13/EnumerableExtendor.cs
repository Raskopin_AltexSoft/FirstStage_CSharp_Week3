﻿using System;
using System.Collections.Generic;


namespace Task13
{
    internal static class EnumerableExtendor
    {
        public static List<T> EnumerableExtend<T>(this IEnumerable<T> enumerable, ICustomCompare customCompare)
        {
            var list = (List<T>)enumerable;
            list.Sort(customCompare.CustomCompare);
            return list;
        }

        public static List<T> EnumerableExtendByDelegate<T>(this IEnumerable<T> enumerable, CustomComparison<T> userCompare)
        {
            var list = (List<T>)enumerable;
            list.Sort(userCompare.Invoke);
            return list;
        }

        public static int ObjectsCompare<T>(T obj1, T obj2)
        {
            return string.Compare(obj1.ToString(), obj2.ToString(), StringComparison.Ordinal);
        }
    }
}
