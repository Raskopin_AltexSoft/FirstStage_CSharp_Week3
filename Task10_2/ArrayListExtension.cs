﻿using System.Collections;
using System.Collections.Generic;


namespace Task10_2
{
    internal static class ArrayListExtension
    {
        public static ArrayList SortBucket(this ArrayList arrayList)
        {
            if (!DoesArrayListIsInt(arrayList) || arrayList.Count < 2)
            {
                return arrayList;
            }
            var maxValue = (int)arrayList[0];
            var minValue = (int)arrayList[0];
            var bucket = new List<int>[maxValue - minValue + 1];
            for (var i = 1; i < arrayList.Count; i++)
            {
                if ((int)arrayList[i] > maxValue)
                    maxValue = (int)arrayList[i];

                if ((int)arrayList[i] < minValue)
                    minValue = (int)arrayList[i];
            }

            

            for (var i = 0; i < bucket.Length; i++)
            {
                bucket[i] = new List<int>();
            }

            foreach (var t in arrayList)
            {
                bucket[(int)t - minValue].Add((int)t);
            }

            var position = 0;

            foreach (var t in bucket)
            {
                if (t.Count <= 0) continue;
                foreach (var t1 in t)
                {
                    arrayList[position] = t1;
                    position++;
                }
            }
            return arrayList;
        }

        public static ArrayList SortSelection(this ArrayList arrayList)
        {
            if (!DoesArrayListIsInt(arrayList))
            {
                return arrayList;
            }

            int min, temp;
            var length = arrayList.Count;

            for (var i = 0; i < length - 1; i++)
            {
                min = i;
                for (var j = i + 1; j < length; j++)
                {
                    if ((int)arrayList[j] < (int)arrayList[min])
                    {
                        min = j;
                    }
                }

                if (min == i) continue;
                temp = (int)arrayList[i];
                arrayList[i] = arrayList[min];
                arrayList[min] = temp;
            }
            return arrayList;
        }

        public static ArrayList SortShaker(this ArrayList arrayList)
        {
            if (!DoesArrayListIsInt(arrayList))
            {
                return arrayList;
            }

            var b = 0;
            var left = 0;
            var right = arrayList.Count - 1;
            while (left < right)
            {
                for (var i = left; i < right; i++)
                {
                    if ((int) arrayList[i] <= (int) arrayList[i + 1]) continue;
                    b = (int)arrayList[i];
                    arrayList[i] = arrayList[i + 1];
                    arrayList[i + 1] = b;
                    b = i;
                }
                right = b;
                if (left >= right) break;
                for (var i = right; i > left; i--)
                {
                    if ((int) arrayList[i - 1] <= (int) arrayList[i]) continue;
                    b = (int)arrayList[i];
                    arrayList[i] = (int)arrayList[i - 1];
                    arrayList[i - 1] = b;
                    b = i;
                }
                left = b;
            }
            return arrayList;
        }

        public static bool DoesArrayListIsInt(ArrayList arrayList)
        {
            var doesArrayListIsInt = true;
            foreach (var element in arrayList)
            {
                if (element is int) continue;
                doesArrayListIsInt = false;
                break;
            }
            return doesArrayListIsInt;
        }
    }
}
