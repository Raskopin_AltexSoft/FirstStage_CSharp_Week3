﻿using System;


namespace Task11_2
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var c = new Complex(20);
            var d = new Complex(20,5);
            var a = c + d;
            Console.WriteLine(a);
            Console.ReadKey();
        }
    }
}
