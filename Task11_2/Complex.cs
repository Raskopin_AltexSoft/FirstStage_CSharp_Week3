﻿using System;


namespace Task11_2
{
    internal class Complex: ICloneable
    {
        double _re;  //real part
        double _im;  //mnemonic part

        public Complex(double r)
        {
            _re = r;
            _im = 0;
        }

        public Complex()
        {
            
        }
        public Complex(double r, double i)
        {
            _re = r;
            _im = i;
        }

        public object Clone()
        {
            var comlexClone = new Complex(_re, _im);
            return comlexClone;
        }

        public static Complex operator +(Complex c1, Complex c2)
        {
            return new Complex(c1._re + c2._re, c1._im + c2._im);
        }

        public static Complex operator -(Complex c1, Complex c2)
        {
            return new Complex(c1._re - c2._re, c1._im - c2._im);
        }

        public static Complex operator *(Complex c1, Complex c2)
        {
            return new Complex(c1._re * c2._re - c1._im * c2._im, c1._re * c2._im + c1._im * c2._re);
        }

        public static Complex operator / (Complex c1, Complex c2)
        {
            var temp = new Complex();
            var r = c2._re*c2._re + c2._im*c2._im;
            temp._re = (c1._re * c2._re + c1._im * c2._im) / r;
            temp._im = (c1._im*c2._re - c1._re*c2._im)/r;
            return temp;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != typeof(Complex)) return false;
            var complex = obj as Complex;
            return complex != null && (Math.Abs(_re - complex._re) < 0.0000001 && Math.Abs(_im - complex._im) < 0.0000001);
        }

        public override int GetHashCode()
        {
            return _re.GetHashCode() ^ _im.GetHashCode();
        }

        public override string ToString()
        {
            return _re + " " + _im;
        }

        public static bool operator ==(Complex c1, Complex c2)
        {
            if ((object)c1 != null && (object)c2 != null && Math.Abs(c1._re - c2._re) < 0.0000001 && Math.Abs(c1._im - c2._im) < 0.0000001)
            {
                return true;
            }
            return false;
        }

        public static bool operator !=(Complex c1, Complex c2)
        {
            return c1 == null || c2 == null || Math.Abs(c1._re - c2._re) > 0.0000001 || Math.Abs(c1._im - c2._im) > 0.0000001;
        }

        public static implicit operator double(Complex c1)
        {
            return c1._re;
        }
    }
}
