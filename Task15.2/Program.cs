﻿using System;
using System.Collections.Generic;

namespace Task4._2
{
    internal class Program
    {
        private static void Main()
        {
            var numberOfStudents = InputNumberOfStudents();
            var students = FillListOfStudents(numberOfStudents);
            OutputStudents(students);
            
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine();
            Console.WriteLine("*successful students*");
            students = Student.GetSuccessfulyStudents(students);
            students.Sort();
            OutputStudents(students);

            Console.ReadKey();
        }
        public static byte InputNumberOfStudents()
        {
            byte numberOfStudents;
            Console.WriteLine("Input number of students:");
            while (!byte.TryParse(Console.ReadLine(), out numberOfStudents))
            {
                Console.WriteLine("Value is wrong. Please, try again:");
            }
            return numberOfStudents;
        }

        public static List<Student> FillListOfStudents(byte studentsNumber)
        {
            var students = new List<Student>();
            for (var i = 0; i < studentsNumber; i++)
            {
                var student = new Student(Faker.Name.First(), Faker.Name.Last())
                {
                    GroupNumber = (byte)Faker.RandomNumber.Next(1, 10)
                };
                SetMarks(student);
                students.Add(student);
            }
            return students;
        }

        public static void SetMarks(Student student)
        {
            var random = new Random((int)DateTime.Now.Ticks);
            for (byte i = 0; i<3; i++)
            {
                if (i<student.ExamenMark.Length)
                {
                    student.SetExamenMark((byte)random.Next(1, 12), i);
                }
            }
        }

        public static void OutputStudents(List<Student> students)
        {
            foreach (Student student in students)
            {
                byte[] marks = student.ExamenMark;
                Console.WriteLine("{0}", student.GetFullName());
                Console.WriteLine("Group number: {0}", student.GroupNumber);
                Console.WriteLine("Examens marks: {0}; {1}; {2}", marks[0], marks[1], marks[2]);
                Console.WriteLine(new string('-', 50));
            }
        }
    }
}
